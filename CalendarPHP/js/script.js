$(function () {
    $("#month_selected").change(function () {
        $.post("php/index.php", {"month" : $("#month_selected").val()},
            function (response) { //success
            $("#calendar").html(response);
      }).fail(function (text, textStatus) {
          $("#letter_text").text(textStatus);
      });
    });
});