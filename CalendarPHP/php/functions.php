<?php
function check_month(int $month) : bool
{
    return ($month > 0 && $month <=12) ? true : false;
}

function build_header(int $month) : string
{
    $calendar_date  = mktime(0, 0, 0, $month, 1, date("Y"));
    return "<p class=h3>" . date("F", $calendar_date)
    . ", " . date("Y", $calendar_date) . "</p><br>";
}

function build_calendar(int $month) : string
{
    $calendar_date  = mktime(0, 0, 0, $month, 1, date("Y"));
    $day_of_week  = (int)date("N", $calendar_date);
    $days_in_month = (int)date("t", $calendar_date);
    $calendar_table =
        <<<TABLE
<table id="calendar_table" class="table">
  <thead>
    <tr class="table-info">
      <th class="text-center">Monday</th>
      <th class="text-center">Tuesday</th>
      <th class="text-center">Wednesday</th>
      <th class="text-center">Thursday</th>
      <th class="text-center">Friday</th>
      <th class="text-center">Saturday</th>
      <th class="text-center">Sunday</th>
    </tr>
  </thead>
  <tbody>
TABLE;
    $weeks_counter = 1;
    for($d = 1, $i = 1; $d <= $days_in_month; $i++) {
        if ($i == 1) {
            $calendar_table .= "<tr>";
        }
        if($weeks_counter != 1 || ($i >= $day_of_week)) {
            $calendar_table .= (($i == 6 || $i == 7) ? "<td class=\"table-danger text-center day-of-month\">$d</td>" : "<td class=\"text-center day-of-month\">$d</td>"); //filled cell
            $d++;
        }
        else
            $calendar_table .= "<td class=\"text-center\"></td>"; //empty cell
        if ($i == 7){
            $weeks_counter++;
            $i = 0;
            $calendar_table .= "</tr>";
        }
    }
    $calendar_table .= "</tbody></table>";
    return $calendar_table;
}