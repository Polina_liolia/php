<?php
require ("functions.php");
if(isset($_POST['month']))
{
    $month = (int)$_POST['month'];
    if (check_month((int)$_POST['month']))
    {
       echo build_header($month);
       echo build_calendar($month);
    }
    else
    {
        echo "Can't build the calendar for unexisting month " . $month;
    }
}