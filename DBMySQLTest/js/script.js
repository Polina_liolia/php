$(function () {
    $.get("server/index.php", {action : "users"}, function (data) {
        $("#users_table").html(data);
        setEventHandlers();
    }).fail(function() {
        console.log( "get request on server failed" );
    });

    $("#add_user_form").submit(function (e) {
        $.post("server/index.php", {user_name: $("#user_name").val(),
            user_login: $("#user_login").val(),
            user_password: $("#user_password").val(),
            user_email: $("#user_email").val(),
            user_phone: $("#user_phone").val()}, function (data) {
            $("#users_table").html(data);
            setEventHandlers();
        } );
        e.preventDefault();
        closeAddUserForm();
    });

    function setEventHandlers() {
        //change user state event handler
        $("#users_table select").on("change", function () {
            $.post("server/index.php", {
                set_state: $("option:selected", this).text(),
                user_id: $(this).closest("tr").attr("id")
            }, function (data) {
                $("#users_table").html(data);
                setEventHandlers();
            }).fail(function () {
                console.log("set state request on server failed");
            });
        });

        //delete user button event handler
        $("#users_table button.btn-danger").click(function () {
            $.post("server/index.php", {delete: $(this).closest("tr").attr("id")}, function (data) {
                $("#users_table").html(data);
                setEventHandlers();
            }).fail(function () {
                console.log("set state request on server failed");
            });
        });

        //add user buton event handler
        $("#btn_add_user").click(function () {
            $(".gray_overlay, #add_user").css("display", "block");
        });

        //close add user form event handler
        $("#close_add_user").click(function () {
            closeAddUserForm();
        });
    }

    function closeAddUserForm() {
        $(".gray_overlay, #add_user").css("display", "none");
        $('#add_user_form')[0].reset();
    }

});
