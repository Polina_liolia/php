<?php

class Users
{
    private $pdo;
    private $insert;
    private $set_state;
    private $select_all;
    private $delete;
    public function __construct($dsn, $user, $password)
    {
        try{
            $this->pdo = new PDO($dsn, $user, $password, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING));
            $this->insert = $this->pdo->prepare("INSERT INTO `Users` (name, login, password, email, phone) VALUES (?, ?, MD5(?), ?, ?)");
            $this->select_all = "SELECT id, name, login, email, phone, state FROM `Users`";
            $this->set_state = $this->pdo->prepare("UPDATE `Users` SET `state` = ? WHERE `Users`.`id` = ?;");
            $this->delete = $this->pdo->prepare("DELETE FROM `Users` WHERE `Users`.`id` = ?;");
        }
        catch (PDOException $e) {
            echo 'Подключение не удалось: ' . $e->getMessage();
        }
    }

    public function insert_user($name, $login, $password, $email, $phone)
    {
        $this->insert->execute(array($name, $login, $password, $email, $phone));
    }

    public function change_user_state($id, $state)
    {
        $this->set_state->execute(array($state, $id));
    }

    public function delete_user($id)
    {
        $this->delete->execute(array($id));
    }

    public function get_all_users()
    {
        return $this->pdo->query($this->select_all);

    }

    public function show_users()
    {
        $select_result = $this->get_all_users();
        $table = <<<TABLE
        <table id="users" class="table">
        <thead class="thead-inverse">
        <tr>
        <th>Name</th>
        <th>Login</th>
        <th>Email</th>
        <th>Phone</th>
        <th>State</th>
        <th>Action</th>
        </tr>
        </thead>
        <tbody>
TABLE;
        foreach ($select_result as $i=>$row)
        {
            $id = $row['id'];
            $table .= "<tr id = \"tr_$id\">";
            $table .= "<td>" . $row['name'] . "</td>";
            $table .= "<td>" . $row['login'] . "</td>";
            $table .= "<td>" . $row['email'] . "</td>";
            $table .= "<td>" . $row['phone'] . "</td>";
            $current_state = $row['state'];
            $other_state = $row['state'] == "inactive" ? "active" : "inactive";
            $table .= <<<SELECT
<td> 
<select class="custom-select">
  <option value="$current_state" selected>$current_state</option>
  <option value="$other_state">$other_state</option>
</select>
 </td>
SELECT;
            $table .= "<td> <button type=\"button\" id=\"btn_$id\" class=\"btn btn-danger\">Delete</button></td>";
            $table .= "</tr>";
        }
        $table .= "</tbody></table>";
        $table .= "<button id='btn_add_user' type=\"button\" class=\"btn btn-primary\">Add user</button>";
        return $table;
    }
}