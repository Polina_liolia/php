<?php
$dsn = 'mysql:dbname=MyDB_polina;host=127.0.0.1';
$user = 'root';
//$password = '123';
$password = '';
//try {
//    $dbh = new PDO($dsn, $user, $password, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING));
//    echo 'Success';
//} catch (PDOException $e) {
//    echo 'Подключение не удалось: ' . $e->getMessage();
//}
//
////preparing query (some kind of draft):
//$insert = $dbh->prepare("INSERT INTO `Users` (name, login, password, email, phone) VALUES (?, ?, MD5(?), ?, ?)");
////executing query with array of params
//$insert->execute(array('Petia', 'ppp1', '123123', 'petr@gmail.com', '+380504587414'));
//$insert->execute(array('Masha', 'mary', '987123', 'mary@ukr.net', '+380689636985'));
//$insert->execute(array('Sasha', 'sasha', '458965', 'alex@mail.ru', '+380675218585'));

require ("Users.php");

$users = new Users($dsn, $user, $password);
if (isset($_GET["action"]))
{
    switch ($_GET["action"])
    {
        case "users":
            echo $users->show_users();
            break;
    }
}

if (isset($_POST["set_state"]))
{
    $state = $_POST["set_state"];
    if (isset($_POST["user_id"])) {
        preg_match("~tr_(\\d+)~", $_POST["user_id"], $matches);
        $id = $matches[1];
        $users->change_user_state($id, $state);
        echo $users->show_users();
        }
}

if (isset($_POST["delete"]))
{
    preg_match("~tr_(\\d+)~", $_POST["delete"], $matches);
    $id = $matches[1];
    $users->delete_user($id);
    echo $users->show_users();
}

if (isset($_POST["user_name"]) && isset($_POST["user_login"]) && isset($_POST["user_password"])
&& isset( $_POST["user_email"]) && isset($_POST["user_phone"]))
{
    $name = $_POST["user_name"];
    $user_login = $_POST["user_login"];
    $user_password = $_POST["user_password"];
    $user_email = $_POST["user_email"];
    $user_phone = $_POST["user_phone"];
    $users->insert_user($name, $user_login, $user_password, $user_email, $user_phone);
    echo $users->show_users();
}



//HW:
//табличка, в которой можно удалить, поменять статус, добавить пользователя
//active record почитать

/*Model
-вывод всех данных
-сохранение одной записи
-обновление (редактирование) одной записи
-выбор записей по условиям
*/