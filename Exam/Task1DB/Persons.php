<?php
class Persons
{
    private $pdo;
    private $select_all;
    public function __construct($dsn, $user, $password)
    {
        try{
            $this->pdo = new PDO($dsn, $user, $password, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING));
            $this->select_all = "SELECT Id, Surname, Birthday FROM `Persons`";
        }
        catch (PDOException $e) {
            echo 'Подключение не удалось: ' . $e->getMessage();
        }
    }

    public function get_all_persons()
    {
        return $this->pdo->query($this->select_all);

    }

    public function show_users()
    {
        $select_result = $this->get_all_persons();
        $table = <<<TABLE
        <table id="persons" class="table">
        <thead class="thead-inverse">
        <tr>
        <th>Id</th>
        <th>Surname</th>
        <th>Birthday</th>
        </tr>
        </thead>
        <tbody>
TABLE;
        foreach ($select_result as $i=>$row) {
            $table .= "<tr>";
            $table .= "<td>" . $row['Id'] . "</td>";
            $table .= "<td>" . $row['Surname'] . "</td>";
            $table .= "<td>" . $row['Birthday'] . "</td>";
            $table .= "</tr>";
        }
        $table .= "</tbody> </table>";
        return $table;
    }
}