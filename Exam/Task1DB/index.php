<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Task1 Persons DB</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css"
          integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
</head>
<body>

</body>
</html>
<?php
$dsn = 'mysql:dbname=Task1_Polina;host=127.0.0.1';
$user = 'root';
$password = '';

require('Persons.php');
$persons = new Persons($dsn, $user, $password);
echo $persons->show_users();