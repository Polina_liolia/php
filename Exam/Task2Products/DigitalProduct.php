<?php

include_once "Product.php";

class DigitalProduct extends Product
{
    private $sizeMB;
    public function __construct($name, $price, $vendor, $discount, $sizeMB)
    {
        parent::__construct($name, $price, $vendor, $discount);
        $this->sizeMB = $sizeMB;
    }
    
    public function __get($property)
    {
        switch ($property)
        {
            case 'name':
                return $this->name;
            case 'price':
                return $this->price;
            case 'vendor':
                return $this->vendor;
            case 'discount':
                return $this->discount;
            case 'sizeMB':
                return $this->sizeMB;
        }
    }

    public function __set($property, $value)
    {
        switch ($property)
        {
            case 'name':
                $this->name = $value;
                break;
            case 'price':
                $this->price = $value;
                break;
            case 'vendor':
                $this->vendor = $value;
                break;
            case 'discount':
                $this->discount = $value;
                break;
            case 'sizeMB':
                $this->sizeMB = $value;
                break;
        }
    }
}