<?php

include_once "Product.php";

class PrintedProduct extends Product
{
    private $author;
    
    public function __construct($name, $price, $vendor, $discount, $author)
    {
        parent::__construct($name, $price, $vendor, $discount);
        $this->author = $author;
    }

    public function __get($property)
    {
        switch ($property)
        {
            case 'name':
                return $this->name;
            case 'price':
                return $this->price;
            case 'vendor':
                return $this->vendor;
            case 'discount':
                return $this->discount;
            case 'author':
                return $this->author;
        }
    }

    public function __set($property, $value)
    {
        switch ($property)
        {
            case 'name':
                $this->name = $value;
                break;
            case 'price':
                $this->price = $value;
                break;
            case 'vendor':
                $this->vendor = $value;
                break;
            case 'discount':
                $this->discount = $value;
                break;
            case 'author':
                $this->author = $value;
                break;
        }
    }
}