<?php

class Product
{
    protected $name;
    protected $price;
    protected $vendor;
    protected $discount;

    public function __construct($name, $price, $vendor, $discount)
    {
        $this->name = $name;
        $this->price = $price;
        $this->vendor = $vendor;
        $this->discount = $discount;
    }

    public function __get($property)
    {
        switch ($property)
        {
            case 'name':
                return $this->name;
            case 'price':
                return $this->price;
            case 'vendor':
                return $this->vendor;
            case 'discount':
                return $this->discount;
        }
    }

    public function __set($property, $value)
    {
        switch ($property)
        {
            case 'name':
                $this->name = $value;
                break;
            case 'price':
                $this->price = $value;
                break;
            case 'vendor':
                $this->vendor = $value;
                break;
            case 'discount':
                $this->discount = $value;
                break;
        }
    }

    public function get_discount_price()
    {
        return $this->price * ((100 - $this->discount)/100);
    }
}