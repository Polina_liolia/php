<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Task1 Persons DB</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css"
          integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
</head>
<body>

</body>
</html>

<?php
include 'PrintedProduct.php';
include 'DigitalProduct.php';

$products_array = [];
$products_array[] = new Product('TV', 15000, 'LG', 15);
$products_array[] = new Product('Laptop', 35000, 'Samsung', 20);
$products_array[] = new PrintedProduct('Stranger in the garden', 350, 'Folio', 7, 'Sven Nurdkvist');
$products_array[] = new PrintedProduct('Birthday pie', 425, 'Folio', 8, 'Sven Nurdkvist');
$products_array[] = new DigitalProduct('Memory card 32GB', 275, 'Logitech', 3, 32000);
$products_array[] = new DigitalProduct('Memory card 16GB', 145, 'Logitech', 5, 16000);

$table = <<<TABLE
        <table id="products" class="table">
        <thead class="thead-inverse">
        <tr>
        <th>Name</th>
        <th>Vendor</th>
        <th>Author</th>
        <th>Size, MB</th>
        <th>Price, UAH</th> 
        <th>Discount, %</th>
        <th>Discount price, UAH</th>
        </tr>
        </thead>
        <tbody>
TABLE;
foreach ($products_array as $i=>$product) {
    $table .= "<tr>";
    $table .= "<td>" . $product->name . "</td>";
    $table .= "<td>" . $product->vendor . "</td>";
    $table .= "<td>" . $product->author . "</td>";
    $table .= "<td>" . $product->sizeMB  . "</td>";
    $table .= "<td>" . $product->price . "</td>";
    $table .= "<td>" . $product->discount . "</td>";
    $table .= "<td>" . $product->get_discount_price() . "</td>";
    $table .= "</tr>";
}
$table .= "</tbody> </table>";

echo $table;