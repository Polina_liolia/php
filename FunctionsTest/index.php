<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
 <!-- require - подключение важных файлов, без которых нельзя работать (столько раз, сколько require записано) -->
<!-- include - подключение файлов, без которых работать можно  (столько раз, сколько include записано) -->
<?php
 
require("./Functions/Library/myLib.php");
printNumbers(1, 100);
?>

</body>
</html>

