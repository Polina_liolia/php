$(function () {
    var products_counter = 1;
    var $tr_draft = $("#order_table tbody tr:first-of-type").clone(true);

    $("#add_product").on("click", function () {
        var new_row = $tr_draft.clone(true).on("input", calcTotalPrice);
        $("td:first-of-type", new_row).text(++products_counter);
        $("#order_table tbody").append(new_row);
    });

    function calcTotalPrice(event) {
        event.stopImmediatePropagation();
        var $parent_tr = $(event.target).closest("tr");
        var total_cost = (Number($("td:nth-of-type(3) input", $parent_tr).val()) *
        Number($("td:nth-of-type(4) input", $parent_tr).val())).toFixed(2);
        $parent_tr.children("td:last-of-type").text(total_cost);

        var $td_sum = $("td:last-of-type", "#order_table tbody tr");
        var total_price = 0;
        $td_sum.each(function (i, el) {
            total_price += Number(el.textContent);
        });
        $("#total_price").text(total_price);
    }

    $("td:nth-of-type(3) input, td:nth-of-type(4) input", "#order_table tbody tr").on("input", calcTotalPrice);

    $("#order_form").on("submit", function (event) {
        event.preventDefault();
        var order_data= Object.create(null);
        Object.defineProperties(order_data,{
            'Order_number' : {
                    value: (Math.random()*1000000000).toFixed(),
                    enumerable: true
                },
            'Name': {
                    value: $("#Name").val(),
                    enumerable: true
                },
            'Surname': {
                    value: $("#Surname").val(),
                    enumerable: true
                },
            'Patronymic': {
                    value: $("#Patronymic").val(),
                    enumerable: true
                },
            'Delivery_address': {
                    value: $("#Delivery_address").val(),
                    enumerable: true
                },
            'Products_range': {
                value: Object.create(null),
                enumerable: true
            },
            'Total_price': {
                    value: $("#total_price").text(),
                    enumerable: true
                }
        });
        $("#order_table tbody tr").each(function (i, el) {
            var arr = Object.create(null);
            Object.defineProperties(arr,{
            'Product': {
                    value: $(el).find("input[name=Product]").val(),
                    enumerable: true
                },
            'Price':  {
                    value: Number($(el).find("input[name=Price]").val()),
                    enumerable: true
                },
            'Amount': {
                    value: Number($(el).find("input[name=Amount]").val()),
                    enumerable: true
                },
            'Sum':  {
                    value: Number($(el).children("td:last-of-type").text()),
                    enumerable: true
                }
            });
            order_data['Products_range'][i] = arr;
        });
        var order_data_string = JSON.stringify(order_data);
        console.log(order_data_string);
        console.log(order_data);

        $.post("php/index.php", {"order": order_data_string},
            function (text) {       //if successfully loaded
                $("#letter_text").html(text);
               // console.log("data sent"+text);
            }//, "json"
        )
            .fail(function (text, textStatus) {
                $("#letter_text").text(textStatus);
            });
        $('#order_form')[0].reset();
    });
});

