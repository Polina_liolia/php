<?php
$letter_draft = <<<LETTER_DRAFT
<div class="container">
    <p>
        Dear [Name] [Patronymic] [Surname]!
        Your order #[Order_number] was successfully registered, our manager will call you as soon as possible.<br>
        Here are details of your order:
    </p>
    [Products_range]
    <p>
    Total price: <b><i>[Total_price] UAH</i></b>.<br>
    Address for delivery: <b><i>[Delivery_address]</i></b>.
    </p>
</div>
LETTER_DRAFT;

function generate_letter($letter, $order_detales)
{
    foreach ($order_detales as $key => $value) {
        if (is_array($value) === false) {   //changing constants in draft with data from the first layer of array
            $letter = str_replace('[' . $key . ']', $value, $letter);
        } else {  //building table using data from inner arrays
            $table_header = <<<HeaderStart
        <table class="table table-bordered table-striped">
        <thead>
        <tr>
        <th>#</th>
HeaderStart;
            foreach ($value[0] as $property_name => $property) {
                $table_header .= "<th>$property_name</th>";
            }
            $table_header .= "</tr></thead>";

            $table_body = "<tbody>";
            foreach ($value as $i => $item) {
                $table_body .= "<tr><td>" . ($i + 1) . "</td>";
                foreach ($item as $property_name => $property) {
                    $table_body .= "<td>$property</td>";
                }
                $table_body .= "</tr>";
            }
            $table_body .= "</tbody></table>";
            $table = $table_header . $table_body;
            $letter = str_replace('[' . $key . ']', $table, $letter);
        }
    }
    return $letter;
}

//sending response to client:
if (isset($_POST['order'])) {
    $order = json_decode($_POST['order'], true);
    $letter = generate_letter($letter_draft, $order);   //generating letter using special function
    echo $letter;

    //sending mail:

    //variant1 - using standard php function:
    $to = "polina.liolia@gmail.com";
    $subject = "New order";
    $message = $letter;
    $headers = 'From: webmaster@example.com' . "\r\n" .
        'Reply-To: webmaster@example.com' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();

    mail($to, $subject, $message, $headers);

    //variant2 - using additional lib:
    //https://github.com/PHPMailer/PHPMailer
    require ("../vendor/phpmailer/phpmailer/src/PHPMailer.php");
    require ("../vendor/phpmailer/phpmailer/src/Exception.php");

    $mail = new \PHPMailer\PHPMailer\PHPMailer(true);
    try{
        //Server settings
       // $mail->SMTPDebug = 2;                                 // Enable verbose debug output
       // $mail->isSMTP();                                      // Set mailer to use SMTP
       // $mail->Host = 'smtp1.example.com;smtp2.example.com';  // Specify main and backup SMTP servers
       // $mail->SMTPAuth = true;                               // Enable SMTP authentication
       // $mail->Username = 'user@example.com';                 // SMTP username
       // $mail->Password = 'secret';                           // SMTP password
       // $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        //$mail->Port = 587;                                    // TCP port to connect to

        //Recipients
        $mail->setFrom('from@example.com', 'Mailer');
        $mail->addAddress('joe@example.net', 'Joe User');     // Add a recipient
        $mail->addAddress('ellen@example.com');               // Name is optional
        $mail->addReplyTo('info@example.com', 'Information');
        $mail->addCC('cc@example.com');
        $mail->addBCC('bcc@example.com');

        //Attachments
        //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'Here is the subject';
        $mail->Body    = $letter;
        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        $mail->send();
        //local mails folder: OpenServer\userdata\temp\email
        echo 'Message has been sent';
    }
    catch(\PHPMailer\PHPMailer\Exception $e )
    {
        echo 'Message could not be sent.';
        echo 'Mailer Error: ' . $mail->ErrorInfo;
    }
}

