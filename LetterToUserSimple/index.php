<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css"
          integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
    <title>Letter creator</title>
</head>
<body>
<div class="container">
    <?php
        $order = [
            'Order_number' => '524585',
            'Name' => 'Anna',
            'Surname' => 'Voronova',
            'Patronymic' => 'Konstantinovna',
            'Delivery_address' => 'Kharkov, Lesnaya Str., 25-4',
            'Products_range' => [
                [
                    'Product' => 'Laptop Lenovo',
                    'Price' => 25470.0,
                    'Amount' => 1,
                    'Sum' => 25470.0
                ],
                [
                    'Product' => 'IPhone 7 32Gb',
                    'Price' => 19850.0,
                    'Amount' => 1,
                    'Sum' => 19850.0
                ],
                [
                    'Product' => 'IPhone USB cable',
                    'Price' => 200.0,
                    'Amount' => 3,
                    'Sum' => 600.0
                ]
            ],
            'Total_price' => 45920.0
        ];

    $letter_draft = <<<LETTER_DRAFT
        <p>
            Dear [Name] [Patronymic] [Surname]!
            Your order #[Order_number] was successfully registered, our manager will call you as soon as possible.<br>
            Here are details of your order:
        </p>
        [Products_range]
        <p>
        Total price: <b><i>[Total_price] UAH</i></b>.<br>
        Address for delivery: <b><i>[Delivery_address]</i></b>.
        </p>
LETTER_DRAFT;

    function generate_letter($letter, $order_detales)
    {
        foreach ($order_detales as $key => $value) {
            if (is_array($value) === false) {   //changing constants in draft with data from the first layer of array
                $letter = str_replace('[' . $key . ']', $value, $letter);
            } else {  //building table using data from inner arrays
                $table_header = <<<HeaderStart
            <table class="table table-bordered table-striped">
            <thead>
            <tr>
            <th>#</th>
HeaderStart;
                foreach ($value[0] as $property_name => $property) {
                    $table_header .= "<th>$property_name</th>";
                }
                $table_header .= "</tr></thead>";

                $table_body = "<tbody>";
                foreach ($value as $i => $item) {
                    $table_body .= "<tr><td>" . ($i + 1) . "</td>";
                    foreach ($item as $property_name => $property) {
                        $table_body .= "<td>$property</td>";
                    }
                    $table_body .= "</tr>";
                }
                $table_body .= "</tbody></table>";
                $table = $table_header . $table_body;
                $letter = str_replace('[' . $key . ']', $table, $letter);
            }
        }
        return $letter;
    }
        echo"<pre>";
        print_r($order);
        echo"</pre>";
        $letter = generate_letter($letter_draft, $order);   //generating letter using special function
        echo $letter;
    ?>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="js/script.js"></script>
</body>
</html>

