$(function () {
    var products_counter = 1;
    var $tr_draft = $("#order_table tbody tr:first-of-type").clone(true);

    $("#add_product").on("click", function () {
        var new_row = $tr_draft.clone(true).on("input", calcTotalPrice);
        $("td:first-of-type", new_row).text(++products_counter);
        $("#order_table tbody").append(new_row);
    });

    function calcTotalPrice(event) {
        event.stopImmediatePropagation();
        var $parent_tr = $(event.target).closest("tr");
        var total_cost = (Number($("td:nth-of-type(3) input", $parent_tr).val()) *
        Number($("td:nth-of-type(4) input", $parent_tr).val())).toFixed(2);
        $parent_tr.children("td:last-of-type").text(total_cost);

        var $td_sum = $("td:last-of-type", "#order_table tbody tr");
        var total_price = 0;
        $td_sum.each(function (i, el) {
            total_price += Number(el.textContent);
        });
        $("#total_price").text(total_price);
    }

    $("td:nth-of-type(3) input, td:nth-of-type(4) input", "#order_table tbody tr").on("input", calcTotalPrice);

    $("#order_form").on("submit", function (event) {
        //event.preventDefault();
        var order_data= [
            {'Order_number': Math.random()},
            {'Name': $("#Name").val()},
            {'Surname': $("#Surname").val()},
            {'Patronymic': $("#Patronymic").val()},
            {'Delivery_address': $("#Delivery_address").val()},
            {'Products_range': []},
            {'Total_price': $("#total_price").text()}
        ];
        $("#order_table tbody tr").each(function (i, el) {
            var arr = [
                {'Product': $(el).find("input[name=Product]").val()},
                {'Price': Number($(el).find("input[name=Price]").val())},
                {'Amount': Number($(el).find("input[name=Amount]").val())},
                {'Sum': Number($(el).children("td:last-of-type").text())}
            ];
            order_data[5].Products_range.push(arr);
        });
        var order_data_string = JSON.stringify(order_data);

        // $.ajax({
        //     url: 'index.php',
        //     type: 'POST',
        //     //dataType: 'json',
        //     data: {"order": order_data_string},
        //     success: function (data) {
        //         console.log(data);
        //     },
        //     error: function (data) {
        //         console.log("Error");
        //     }
        // });

        $.post("index.php", {"order": order_data_string},
            function (text) {       //if successfully loaded
                console.log("data sent"+text);
            }//, "json"
        )
            .fail(function (text, textStatus) {
                console.log(textStatus);
            });
    });
});

