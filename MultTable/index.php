<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css"
          integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
    <title>MultTable</title>
</head>
<body>
<!--using for-->
<p class="h2">MultTable1</p>
<?php
echo "<table class=\"table table-striped table-inverse\">";
for ($i = 1; $i < 10; $i++)
{
    echo "<tr>";
    for ($j = 1; $j < 10; $j++)
        echo "<td> $j x $i = " . $j*$i . "</td>";
    echo "</tr>";
}
echo "</table>";
?>
<!--using foreach-->
<p class="h2">MultTable2</p>
<?php
echo "<table  class=\"table table-striped\">";
$numbers = array(1, 2, 3, 4, 5, 6, 7, 8, 9 );
echo "<td><b> </b></td>";
foreach($numbers as $val)
    echo "<td><b>". $val . "</b></td>";
foreach ($numbers as $val1)
{
    echo "<tr>";
    echo "<td><b>". $val1 . "</b></td>";
    foreach($numbers as $val2)
        echo "<td>" . $val2 * $val1 . "</td>";
    echo "</tr>";
}
echo "</table>";
?>
</body>
</html>
