<?php

function getArr()
{
    $array = [
        "foo" => "bar",
        "bar" => "foo",
    ];
    return $array;
}

$array = getArr();
unset($array["foo"]);//delete element
var_dump($array);

//переиндексировать массив:
$a = array(1 => 'один', 2 => 'два', 3 => 'три');
unset($a[2]);
/* даст массив, представленный так:
   $a = array(1 => 'один', 3 => 'три');
   а НЕ так:
   $a = array(1 => 'один', 2 => 'три');
*/

$b = array_values($a);
// Теперь $b это array(0 => 'один', 1 => 'три')

// ассоциативность тернарного оператора отличается от  C/C++
$a = true ? 0 : true ? 1 : 2; // (true ? 0 : true) ? 1 : 2 = 2


//Пример #1 Использование global

$a = 1;
$b = 2;

function Sum()
{
    global $a, $b;

    $b = $a + $b;
}

Sum();
echo $b;
