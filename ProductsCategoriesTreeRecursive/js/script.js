$(function ()
{
    $.get("php/index.php", {catalog_html : "catalog_html"}, function (data) {
        console.log(data);
        $('#jstree').html(data);

        $('#jstree').jstree({
            "core": {
                "animation": 0,
                "check_callback": true,
                "themes": {"stripes": true},
            },
            "plugins": [
                "contextmenu", "dnd", "search",
                "state", "types", "wholerow", "themes", "ui"
            ]
        });
    });

});