<?php
class Product
{
    private static $counter = 1;
    private $id;
    private $categoryID;
    private $name;

    public function ID(int $aID=null)
    {
        if ($aID === null)
            return $this->id;
        else
            $this->id = $aID;
    }

    public function CategoryID(int $aCatID=null)
    {
        if ($aCatID === null)
            return $this->categoryID;
        else
            $this->categoryID = $aCatID;
    }

    public function Name(string $aName=null)
    {
        if ($aName === null)
           return  $this->name;
        else
            $this->name = $aName;
    }

    public function __construct(int $aCatID,string $aName )
    {
        $this->id = Product::$counter++;
        $this->categoryID = $aCatID;
        $this->name = $aName;
    }

}

class Category
{
    private static $counter = 1;
    private $id;
    private $parentID;
    private $categoryName;

    public function __construct(int $aParentID, string $aCategoryName )
    {
        $this->id = Category::$counter++;
        $this->parentID = $aParentID;
        $this->categoryName = $aCategoryName;
    }

    public function ID(int $aID=null)
    {
        if ($aID === null)
            return $this->id;
        else
            $this->id = $aID;
    }

    public function ParentID(int $aParentID=null)
    {
        if ($aParentID === null)
            return $this->parentID;
        else
            $this->parentID = $aParentID;
    }

    public function CategoryName(string $aCategoryName=null)
    {
        if ($aCategoryName === null)
            return $this->categoryName;
        else
            $this->categoryName = $aCategoryName;
    }
}


class Catalog
{
    private $allCategories = [];
    private $allProducts = [];
    private $catalog = [];

    public function Catalog()
    {
        return $this->catalog;
    }

    public function __construct(array $aAllCategories = null, array $aAllProducts=null)
    {
        $this->addCategoriesRange($aAllCategories);
        $this->addProductsRange($aAllProducts);
    }


    private function HTMLhelper(array $categoryArr, string $categoryName = null) : string
    {
        $categoryHTML = "<li>";
        $categoryHTML .= $categoryName ? $categoryName : "";
        $categoryHTML .= "<ul>";
        foreach ($categoryArr as $i=>$item)
        {
            if (is_array($item))
                $categoryHTML .= $this->HTMLhelper($item, $i);
            else if ($item instanceof Product)
                $categoryHTML .= "<li data-jstree='{\"icon\":\"jstree-file\"}'>" . $item->Name() . "</li>";
        }
        $categoryHTML .= "</ul></li>";
        return $categoryHTML;
    }

    public function createCatalogHTML() : string
    {
        $catalogHTML = "<ul>" . $this->HTMLhelper($this->catalog) . "</ul>";
        return $catalogHTML;
    }
    //https://www.jstree.com/

    //recursive method to use inside class
    private function addCategoryToCatalog(array &$arr, Category $category) : bool
    {
        if ($category->ParentID() == -1) //first-stage category
        {
            $arr[$category->CategoryName()] = [];
            return true; //category array created
        }
        else
        {
            $parentName = $this->allCategories[$category->ParentID()]->CategoryName();
            if (key_exists($parentName, $arr))
            {
                $arr[$parentName][$category->CategoryName()] = [];
                return true; //category array created
            }
            else
                foreach ($arr as $i=>$subitem)
                    if (is_array($subitem))
                    {
                        $result = $this->addCategoryToCatalog($arr[$i], $category);
                        if ($result)
                            return true;
                    }
        }
        return false;
    }



    private function addProductToCatalog(array &$arr, Product $p) : bool
    {
        if ($p->CategoryID() == -1) //product without category
        {
            $arr[$p->ID()] = $p;
            return true; //product added to the root of catalog
        }
        else
        {
            $parentName = $this->allCategories[$p->CategoryID()]->CategoryName();
            if (key_exists($parentName, $arr))
            {
                $arr[$parentName][$p->ID()] = $p;
                return true; //product added to the parent category
            }
            else
                foreach ($arr as $i=>$subitem)
                    if (is_array($subitem))
                    {
                        $result = $this->addProductToCatalog($arr[$i], $p);
                        if ($result)
                            return true;
                    }
        }
        return false;
    }

    public function addCategory(Category $c) : bool
    {
        if(array_key_exists($c->ID(), $this->allCategories))
            return false;//such category already exists
        if (!array_key_exists($c->ParentID(), $this->allCategories) && $c->ParentID() !== -1)
            return false;//no such a parent category exists
        $this->allCategories[$c->ID()] = $c;
        $categoryAdded = $this->addCategoryToCatalog($this->catalog, $c);
        return $categoryAdded;
    }

    public function addCategoriesRange($c) : bool
    {
        if ($c === null)
            return false;
        $someCategoriesAdded = false;
        foreach ($c as $cat_item)
        {
            $result = $this->addCategory($cat_item);
            if ($result !== false)  //category successfully added
                $someCategoriesAdded = true;
        }
        return $someCategoriesAdded;
    }

    //recursive removing category and all it's subitems (to use inside class)
    private function removeCategoryFromCatalog(array &$arr, Category $category) : bool
    {
        if (key_exists($category->CategoryName(), $arr))
        {
            foreach ($arr[$category->CategoryName()] as $i => $item) {
                if ($item instanceof Product)
                {
                    unset($arr[$category->CategoryName()][$i]); //removing inner product from catalog
                    unset($this->allProducts[$item->ID()]); //removing inner product from allProducts array
                }
                else if (is_array($item)) //item is category array
                {
                    //searching for category key in all categories array:
                    $categoryToRemoveKey = -1;
                    foreach ($this->allCategories as $j=>$cat)
                        if ($cat->CategoryName() == $i)
                        {
                            $categoryToRemoveKey = $j;
                            break;
                        }
                    if ($categoryToRemoveKey == -1)
                        continue;
                    $this->removeCategoryFromCatalog($arr[$category->CategoryName()], $this->allCategories[$categoryToRemoveKey]);  //removing inner category from catalog
                }
            }
            unset( $arr[$category->CategoryName()]); //removing category from catalog
            unset( $this->allCategories[$category->ID()]); //removing category from allCategories array
            return true;
        }
        else
        {
            foreach ($arr as $i=>$subitem)
                if (is_array($subitem))
                {
                    $result = $this->removeCategoryFromCatalog($arr[$i], $category);
                    if ($result)
                        return true;
                }
        }
        return false;
    }

    //removes category and all subcategories and products in it
    public function removeCategoryAndSubcategories(int $categoryID) : bool
    {
        $categoryToRemove = $this->allCategories[$categoryID];
        if ($categoryToRemove === null)
            return false;
        $removed = $this->removeCategoryFromCatalog($this->catalog, $categoryToRemove);
        return $removed;
    }

    public function addProduct(Product $p) : bool
    {
        if (array_key_exists($p->ID(), $this->allProducts))
            return false; //can't add a product: such product already exists
        //checking if such category exists:
        if (array_key_exists($p->CategoryID(), $this->allCategories))
        {
            $this->allProducts[$p->ID()] = $p;
            $productAddedToCatalog = $this->addProductToCatalog($this->catalog, $p);
            return $productAddedToCatalog;
        }
        return false; //can't add a product: the category of this product is absent in allCategories
    }

    public function addProductsRange($p) : bool
    {
        if ($p === null)
            return false;
        $someProductsAdded = false;
        //standardizing an array:
        foreach($p as $i=>$item)
        {
            $result = $this->addProduct($item);
            if($result !== false)   //some product was successfully added
                $someProductsAdded = true;
        }
        return $someProductsAdded;
    }

    //recursive removing product (to use inside class)
    private function removeProductFromCatalog(array &$arr, Product $p) : bool
    {
        $parentCategoryName = $this->allCategories[$p->CategoryID()]->CategoryName();
        if (key_exists($parentCategoryName, $arr))
        {
            unset($arr[$parentCategoryName][$p->ID()]);//removing product from catalog
            unset($this->allProducts[$p->ID()]); //removing product from allProducts array
            return true;
        }
        else
        {
            foreach ($arr as $i=>$subitem)
                if (is_array($subitem))
                {
                    $result = $this->removeProductFromCatalog($arr[$i], $p);
                    if ($result)
                        return true;
                }
        }
        return false;
    }

    public function removeProduct(int $productID) : bool
    {
        $p = $this->allProducts[$productID];
        if(!$p)
            return false; //wong product index
        $productRemoved = $this->removeProductFromCatalog($this->catalog, $p);
        return $productRemoved;
    }
}



