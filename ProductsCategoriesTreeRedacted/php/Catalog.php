<?php

require ("TreeItem.php");

class Catalog
{
    private static $catalogItemID = 1;
    private $items = [];

    public function __construct()
    {}

    public function createCatalogTree() : string
    {
        $arr = [];

        foreach ($this->items as $i => $item)
        {
            $parentCatalogID = !$item->parent ? "#" : $item->parent; //checking if parent is a root
            $item->parent = $parentCatalogID;
            array_push($arr, $item);
        }
        return json_encode($arr);
    }

    //adds the category (if it has valid parent)
    public function addCategory($name, $parentId = 0)
    {
        if ($parentId && !array_key_exists($parentId, $this->items))
            return false;//no such a parent category exists

        $id = Catalog::$catalogItemID++;
        $this->items[$id] = new TreeItem($id,$name, $parentId);
        return $id;
    }

    //removes category and all subcategories and products in it
    public function removeCategory(int $categoryID) : bool
    {
        if (!key_exists($categoryID, $this->items))
            return false; //no item with such ID exists
        unset($this->items[$categoryID]);
        foreach ($this->items as $i=>$item)
        {
            if ($item->parent === $categoryID)
                $this->removeCategory($i);
        }
        return true;
    }

    public function renameCategory(int $categoryID, string $newName) : bool
    {
        if (!key_exists($categoryID, $this->items))
            return false; //no item with such ID exists
        $this->items[$categoryID]->text = $newName;
        return true;
    }

}



