<?php

require("Catalog.php");

$catalog = new Catalog();

$parent1Id = $catalog->addCategory("Computers");
$parent2Id = $catalog->addCategory("Printers");
$parent3Id = $catalog->addCategory("Computer accessorise");

$parent2_1Id = $catalog->addCategory("PCs", $parent1Id);
$catalog->addCategory("Laptops", $parent1Id);
$catalog->addCategory("Laser printers", $parent2Id);
$catalog->addCategory("Matrix printers", $parent2Id);

$catalog->addCategory("Keypads", $parent3Id);
$catalog->addCategory("Mouses", $parent3Id);
$catalog->addCategory("Web-cameras", $parent3Id);

$catalog->addCategory("Monoblocks", $parent2_1Id);
$catalog->addCategory("Home PCs", $parent2_1Id);

/*
echo "<pre>";
var_dump($catalog);
echo "</pre>";
*/

/* tests:
echo $catalog->removeCategory(1);
echo $catalog->removeCategory(25);
echo $catalog->removeCategory(2);


echo "<pre>";
var_dump($catalog);
echo "</pre>";
*/

if(isset($_GET["id"]))
{
    header('Content-Type: application/json; charset=utf-8');
    echo $catalog->createCatalogTree();
}

