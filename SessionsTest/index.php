<?php
session_start(); //cookie will be set by default
if (!isset($_SESSION['count'])) {
    $_SESSION['count'] = 0;
} else {
    $_SESSION['count']++;
}
//PHPSESID


setcookie("mySessionId", "", time() - 3600); //delete cookie
setcookie("mySessionId", //name
    "gnjtgntjenl5656r4egr562egrge", //value
    time() + 3600, //time live 1 hour //if not set - browser sessoin live time
    "/~rasmus/", //Путь к директории на сервере, из которой будут доступны cookie
    "example.com", //domain
    1 //httponly (only for php, invisible for js scripts)
);

echo $_COOKIE["mySessionId"] . "<br>"; //to get one cookie by name
print_r($_COOKIE); //to get all cookies

// Удаляем все переменные сессии.(обязательно!!!!!!!)
$_SESSION = array();

//не обязательно:
// Если требуется уничтожить сессию, также необходимо удалить сессионные cookie.
// Замечание: Это уничтожит сессию, а не только данные сессии!
if (ini_get("session.use_cookies")) {
    $params = session_get_cookie_params();
    setcookie(session_name(), '', time() - 42000,
        $params["path"], $params["domain"],
        $params["secure"], $params["httponly"]
    );
}

//Обязательно!!!!!!!!!!!!!!
// Наконец, уничтожаем сессию.
session_destroy();

