<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css"
          integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <title>LogIn</title>
</head>
<body>
<!--Дана форма ввода логина и пароля.
Реализовать проверки:
Логин и пароль - не короче 6 символов
Логин должен иметь большие и маленькие буквы
Логин не может иметь никаких символов, кроме букв и цифр
Пароль обязательно должен содержать буквы в разных регистрах, цифры и знаки препинания
-->
<div class="container">
    <div class="errors">
<?php
$lowercase = "abcdefghijklmnopqrstuvwxyz";
$uppercase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
$numbers = "1234567890";
$punctuation_symbols = '!\'"#$;:%^&*()-_=+/.,@|';
if(isset($_POST['user_login']))
{
    $login = $_POST['user_login'];
    if (strlen($login) === 0)
        echo "User login can not be empty.<br>";
    if (strlen($login) < 6)
        echo "Login must contain 6 or more symbols.<br>";
    if (strpbrk($login, $lowercase) === false || strpbrk($login, $uppercase) === false)
        echo "Login must contain lower- and uppercase symbols.<br>";
    if (strpbrk($login, $punctuation_symbols) !== false)
        echo "Login can not contain punctuational symbols.<br>";
}

if(isset($_POST['user_password']))
{
    $password = $_POST['user_password'];
    if (strlen($password) === 0)
        echo "Password can not be empty.<br>";
    if (strlen($password) < 6)
        echo "Password must contain 6 or more symbols.<br>";
    if (strpbrk($password, $lowercase) === false || strpbrk($password, $uppercase) === false)
        echo "Password must contain lower- and uppercase symbols.<br>";
    if (strpbrk($password, $punctuation_symbols) === false)
        echo "Password must contain punctuational symbols.<br>";
    if (strpbrk($password, $numbers) === false)
        echo "Password must contain numbers.<br>";
}
?>
    </div>
    <form method="POST" action="?">
        <div class="form-group">
            <label for="user_login">Login</label>
            <input type="text" class="form-control" name="user_login" id="user_login" aria-describedby="loginHelp" placeholder="Login">
            <small id="loginHelp" class="form-text text-muted">Login contains 6 and more characters and numbers, lower- and uppercase</small>
        </div>
        <div class="form-group">
            <label for="user_password">Password</label>
            <input type="password" class="form-control" name="user_password" id="user_password" aria-describedby="passwordHelp" placeholder="Password">
            <small id="passwordHelp" class="form-text text-muted">Password must contain 6 and more symbols, including lower- and uppercase characters, numbers and punctuation symbols</small>
        </div>
        <input type="submit" class="btn btn-primary" value="Submit">
    </form>
</div>
</body>
</html>
