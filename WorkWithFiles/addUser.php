<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css"
          integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <title>Register user</title>

</head>
<body>
<div class="container">
    <form id="reg_form" method="POST" action="index.php">
        <div class="form-group">
            <label for="user_login">Login</label>
            <input type="text" class="form-control" name="user_login" id="user_login" aria-describedby="loginHelp" placeholder="Login">
            <small id="loginHelp" class="form-text text-muted">Login contains 3-20 symbols</small>
        </div>
        <div class="form-group">
            <label for="user_email">Email address</label>
            <input type="email" class="form-control" name="user_email" id="user_email" placeholder="Email">
        </div>
        <div class="form-group">
            <label for="user_password">Password</label>
            <input type="password" class="form-control" name="user_password" id="user_password" aria-describedby="passwordHelp" placeholder="Password">
            <small id="passwordHelp" class="form-text text-muted">Password must contain more than 6 symbols, including both characters and numbers </small>
        </div>
        <div class="form-group">
            <label for="confirm_password">Repeat password</label>
            <input type="password" class="form-control" name="confirm_password" id="confirm_password" placeholder="Password">
        </div>
        <input type="submit" class="btn btn-primary" value="Submit">
    </form>
</div>
</body>
</html>

