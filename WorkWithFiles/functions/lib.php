<?php
function count_users($source)
{
    if (file_exists($source))
    {
        $arr = file($source);
        return count($arr);
    }
    else
        return 0;
}

function check_login($source, $login)
{
    if (file_exists($source)) { //checking if file exists
        $all_users = file_get_contents($source);
        if ($all_users && strpos($all_users, $login . ': ') !==false) //string contains login
            return false; //such login already exists
    }
    return true; //login is available
}

