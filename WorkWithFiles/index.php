<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css"
          integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
    <title>Menu</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<div class="container">
    <ul class="nav nav-pills nav-fill">
        <li class="nav-item">
            <a class="nav-link" href="?action=add_user">Add user</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="?action=show_users">Show users</a>
        </li>
        <li class="nav-item">
            <?php
            require("functions/lib.php");
            $users_number = count_users("users.txt");
            echo <<<EOT
    <span class="users_count badge badge-info">Users registered: $users_number</span>
EOT;
            ?></li>
    </ul>
    <?php
    if(isset($_GET["action"]))
        switch($_GET["action"])
        {
            case "add_user":
            {
                require("addUser.php");
                break;
            }
            case "show_users":
            {
                require("showUsers.php");
                break;
            }
        }
//    require("functions/lib.php");
    if(isset($_POST['user_login']))
    {
        if (check_login("users.txt", $_POST['user_login']))
        {
            $fp = fopen("users.txt", "at"); // open to write to the end of file in a text mode
            if (!$fp)
                $fp = fopen("users.txt", "w"); // creating a new file to write
            $text =  $_POST['user_login'] . ': ' . $_POST['user_email'] . ': ' . $_POST['user_password'] . "\r\n";
            $result = fwrite($fp, $text); // writing text
            if ($result)
            {
                echo <<<EOT
<div class="alert alert-success" role="alert">
Thank you! Your data has been successfully saved!
</div>
EOT;
            }
            else {
                echo <<<EOT
<div class="alert alert-warning" role="alert">
  Writing to file error!
</div>
EOT;
            }
            fclose($fp);
        }
        else
        {
            echo <<<EOT
<div class="alert alert-warning" role="alert">
  Such login already exists! Choose another one and try again.
</div>
EOT;
            require("addUser.php");
        }
    }
    ?>

</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="bower_components/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="js/script.js"></script>
</body>
</html>


