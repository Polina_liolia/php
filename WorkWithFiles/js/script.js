
 $(document).ready(function(){
     jQuery.validator.addMethod("check_password", function(value) {
         return value == $("#user_password").val();
     }, "Please input the same password");

     $("#reg_form").validate({

         rules:{
             user_login:{
                required: true,
                 minlength: 3,
                 maxlength: 20

            },
             user_email:{
                 required: true,
                 email: true
             },
             user_password:{
                 required: true,
                 minlength: 6
             },
             confirm_password:{
                 check_password: true
             }
         },

         messages:{
             user_login:{
                 required: "Login can't be empty!"
             },
             user_email:{
                 required: "Email can't be empty!",
                 email: "Email is not correct!"
             },
             user_password:{
                 required: "Password can't be empty!"
             }
         }

     });

 });


