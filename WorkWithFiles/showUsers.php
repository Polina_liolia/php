<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css"
          integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <title>Show users</title>
</head>
<body>
<div class="container">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>#</th>
            <th>Login</th>
            <th>E-mail</th>
            <th>Password</th>
        </tr>
        </thead>
        <?php
        if (file_exists("users.txt"))
        {
            $arr = file("users.txt");

            foreach ($arr as $i=>$element)
            {

                echo "<tr><th scope=\"row\">" . (string)($i+1) . "</th>";
                $user_data_array = explode(": ", $element);
                foreach ($user_data_array as $val)
                    echo "<td>$val</td>";
                echo "</tr>";
            }
        }
        ?>
    </table>
</div>
</body>
</html>