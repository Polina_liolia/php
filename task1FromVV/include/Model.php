<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 27.09.2017
 * Time: 11:50
 */
$dsn = 'mysql:dbname=framework1;host=localhost';
$user = 'root';
$password = '';

$dbh = new PDO($dsn, $user, $password,array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING));

abstract class Model
{
    private $data = [];
    private $is_new = false;

    abstract protected function table();

    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }
    public function __get($name)
    {
        return $this->data[$name];
    }

    public function create($fields){
        foreach ($fields as $k=>$v){
            $this->$k = $v;
        }
    }

    public function all(){
        global $dbh;
        $sql = "SELECT * FROM `".$this->table()."`";
        $st = $dbh->query($sql);
        $st->setFetchMode(PDO::FETCH_ASSOC);
        $results = [];
        foreach ($st as $v){
            $results[] = new static;
            end($results)->create($v);
        }
        return $results;
    }

    public function save(){
        global $dbh;
        $sql = "";
        if(!$this->is_new){
            $sql = "UPDATE ".$this->table()." SET (";
        }
        $tmp_data = array_splice($this->data,1);

        $keys = "`".implode("`,`", array_keys($tmp_data))."`";
        $values = "'".implode("','", $tmp_data)."'";

        $sql .= "$keys) VALUES ($values) WHERE `id` = '{$this->data['id']}'";

        $dbh->query($sql);
        //echo $sql;
    }
}