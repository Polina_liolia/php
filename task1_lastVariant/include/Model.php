<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 27.09.2017
 * Time: 11:50
 */
$dsn = 'mysql:dbname=framework1;host=localhost';
$user = 'root';
$password = '';

$dbh = new PDO($dsn, $user, $password,array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING));

abstract class Model
{
    private $data = [];
    private $is_new = false;

    public function __construct()
    {
        $this->is_new = true;
    }

    abstract protected function table();

    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }
    public function __get($name)
    {
        return $this->data[$name];
    }

    public function create($fields){

        foreach ($fields as $k=>$v){
            $this->$k = $v;
        }
        return $this;
    }

    public function all(){
        global $dbh;
        $sql = "SELECT * FROM `".static::table()."`";
        $st = $dbh->query($sql);
        $st->setFetchMode(PDO::FETCH_ASSOC);
        $results = [];
        foreach ($st as $v){
            $results[] = new static;
            end($results)->create($v)->is_new = false;

        }
        return $results;
    }

    public function save(){
        global $dbh;
        $sql = "";

        $tmp_data = array_slice($this->data,1);

        if(!$this->is_new){
            $sql = "UPDATE ".$this->table()." SET ";
            $values = [];
            foreach($tmp_data as $k=>$v){
                $values[] = "`$k` = '$v'";
            }
            $sql .= implode(",", $values);
            $sql .= " WHERE `id`='{$this->id}' ";


        }else{
            $sql = "INSERT INTO ".$this->table()." (";
            $keys = "`".implode("`,`", array_keys($tmp_data))."`";
            $values = "'".implode("','", $tmp_data)."'";

            $sql .= "$keys) VALUES ($values) ";

        }
      $dbh->query($sql);
        //echo $sql;
    }

    static public function find($id)
    {
        global $dbh;

        $st = $dbh->prepare("SELECT * FROM " . static::table() . " WHERE `id` = ?");
        $st->bindValue(1, $id, PDO::PARAM_INT);
        $st->execute();

        if (FALSE !== ($result = $st->fetch(PDO::FETCH_ASSOC))) {
            $inst = new static;
            $inst->create($result);
            $inst->is_new = false;

            return $inst;
        }
        throw new Exception("No record found");
    }
}